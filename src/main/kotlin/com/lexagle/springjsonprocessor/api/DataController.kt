package com.lexagle.springjsonprocessor.api

import com.lexagle.springjsonprocessor.service.DataService
import org.springframework.core.io.Resource
import org.springframework.core.io.UrlResource
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpStatus
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.servlet.support.ServletUriComponentsBuilder
import java.io.IOException
import java.lang.IllegalArgumentException
import java.net.MalformedURLException
import java.nio.file.Files
import java.nio.file.Paths
import java.nio.file.StandardCopyOption
import kotlin.io.path.Path
import kotlin.io.path.exists

@RestController
@RequestMapping("/test-data-group")
class DataController {
    private val dataService: DataService

    constructor(dataService: DataService) {
        this.dataService = dataService
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    fun uploadZip(@RequestParam("file") file: MultipartFile): ResponseEntity<String> {
        val fileName = file.originalFilename ?: throw IllegalArgumentException("Invalid filename. File not found.")
        val storagePath = Paths.get("./src/main/resources/storage/")

        if (!storagePath.exists()) {
            Files.createDirectory(storagePath)
        }

        val pathDirectory = Path("$storagePath/$fileName")

        try {
            Files.copy(file.inputStream, pathDirectory, StandardCopyOption.REPLACE_EXISTING)
        } catch (e: IOException) {
            e.printStackTrace()
        }

        val fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
            .path("/files/download/")
            .path(fileName)
            .toUriString()

        return ResponseEntity.ok(fileDownloadUri)
    }

    @GetMapping
    fun downloadZip(): ResponseEntity<Resource> {
        dataService.processData()
        val path = Path("./src/main/resources/storage/test-data-group.zip")

        return try {
            val resource = UrlResource(path.toUri())
            ResponseEntity.ok()
                .contentType(MediaType.parseMediaType("application/zip"))
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.filename + "\"")
                .body(resource)
        } catch (e: MalformedURLException) {
            e.printStackTrace()

            throw MalformedURLException()
        }
    }
}
