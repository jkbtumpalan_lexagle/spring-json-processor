package com.lexagle.springjsonprocessor

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration
import org.springframework.boot.runApplication

@SpringBootApplication(exclude = [DataSourceAutoConfiguration::class])
class SpringJsonProcessorApplication

fun main(args: Array<String>) {
	runApplication<SpringJsonProcessorApplication>(*args)
}