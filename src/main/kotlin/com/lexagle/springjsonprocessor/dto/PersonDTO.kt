package com.lexagle.springjsonprocessor.dto

import com.fasterxml.jackson.annotation.JsonProperty

data class PersonDTO(
    val id: Int,
    @JsonProperty("first_name")
    val firstName: String,
    @JsonProperty("last_name")
    val lastName: String,
    val email: String,
    val gender: String,
    @JsonProperty("ip_address")
    val ipAddress: String,
    @JsonProperty("favorite_animal")
    val favoriteAnimal: String,
    @JsonProperty("favorite_car_brand")
    val favoriteCarBrand: String,
    @JsonProperty("favorite_color")
    val favoriteColor: String,
    @JsonProperty("favorite_drug")
    val favoriteDrug: String,
    val race: String,
    val country: String
)
