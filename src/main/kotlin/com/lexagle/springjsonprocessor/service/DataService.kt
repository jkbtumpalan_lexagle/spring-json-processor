package com.lexagle.springjsonprocessor.service

import com.lexagle.springjsonprocessor.dto.PersonDTO
import com.lexagle.springjsonprocessor.utility.FileHandler
import com.lexagle.springjsonprocessor.utility.ZipHandler
import kotlinx.coroutines.runBlocking
import org.springframework.stereotype.Service
import java.nio.file.Files
import java.nio.file.Paths
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Value
import kotlin.io.path.exists
import kotlin.system.measureTimeMillis

@Service
class DataService(
    private val fileHandler: FileHandler,
    @Value("\${mock.data.dir}") private val mockDataSource: String,
    @Value("\${unzipped.data.dir}") private val source: String,
    @Value("\${output.data.dir}") private val output: String,
) {

    fun processData() {
        val outputPath = Paths.get(output)

        if (!outputPath.exists()) {
            Files.createDirectory(outputPath)
        }
        ZipHandler.unzipFile(mockDataSource, source)

        var mainData: List<PersonDTO>

        runBlocking {
            // Read unzipped data and measure time
            val time = measureTimeMillis {
                mainData = fileHandler.loadAllResourceData(source)
            }

            println("File reading of all files completed within $time ms")

            fileHandler.serializeResources(output, mainData)
        }

        // Separate the combined file into multiple json files (grouped by country)
        runBlocking {
            val time = measureTimeMillis {
                fileHandler.serializeResourcesByCountry(output, mainData.groupBy { it.country })
            }
            println("Finished serialization after grouping by country within $time ms")

            ZipHandler.zipAll(output, "src/main/resources/storage/test-data-group.zip")
        }
    }
}
