package com.lexagle.springjsonprocessor.utility

import com.fasterxml.jackson.core.util.DefaultPrettyPrinter
import com.fasterxml.jackson.databind.ObjectWriter
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class JacksonConfig {
    @Bean
    fun mapper() = jacksonObjectMapper()

    @Bean
    fun writer(): ObjectWriter = mapper().writer(DefaultPrettyPrinter())
}
