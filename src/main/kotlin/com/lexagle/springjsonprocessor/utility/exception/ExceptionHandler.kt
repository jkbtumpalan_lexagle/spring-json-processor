package com.lexagle.springjsonprocessor.utility.exception

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.RestControllerAdvice
import java.lang.IllegalArgumentException

@RestControllerAdvice
class ExceptionHandler {

    @ExceptionHandler(value = [IllegalArgumentException::class])
    fun generalExceptionHandler(exception: IllegalArgumentException): ResponseEntity<ApiError> {
        val error = ApiError(exception.message, HttpStatus.BAD_REQUEST)
        return ResponseEntity(error, error.status)
    }
}