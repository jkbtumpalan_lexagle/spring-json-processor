package com.lexagle.springjsonprocessor.utility.exception

import org.springframework.http.HttpStatus

data class ApiError(
    private val message: String? = "Something went wrong.",
    val status: HttpStatus = HttpStatus.BAD_REQUEST,
    val code: Int = status.value()
)
