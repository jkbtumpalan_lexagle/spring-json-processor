package com.lexagle.springjsonprocessor.utility
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.ObjectWriter
import com.fasterxml.jackson.module.kotlin.readValue
import com.lexagle.springjsonprocessor.dto.PersonDTO
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.launch
import org.springframework.stereotype.Service
import java.io.File

@Service
class FileHandler(
    private val mapper: ObjectMapper,
    private val writer: ObjectWriter,
){

    private fun readFileUsingBufferedReader(fileName: String): List<PersonDTO> =
        mapper.readValue(File(fileName).bufferedReader().readText())

    private fun writeFilesUsingObjectWriter(directory: String, country: String?, jsonArray: List<PersonDTO>) {
        // Conditional File Name for Writing the Combined Data and the Grouped/Sorted Data
        val fileName = country ?: "00_COMBINED_MOCK_DATA"
        writer.writeValue(File("$directory/$fileName.json"), jsonArray)
    }

    suspend fun loadAllResourceData(directory: String): List<PersonDTO> {
        val resourceDirectory = File(directory)
        val resourceList = resourceDirectory.listFiles() as Array<File>
        val mockInputData = mutableListOf<PersonDTO>()

        coroutineScope {
            for (file_ in resourceList) {
                // Launch a coroutine to read each file.
                launch {
                    if (file_.isFile && !file_.isHidden && file_.extension == "json") {
                        mockInputData.addAll(
                            readFileUsingBufferedReader("$directory/${file_.name}")
                        )
                    }
                }
            }
        }

        return mockInputData
    }

    fun serializeResources(directory: String, data: List<PersonDTO>) {
        writeFilesUsingObjectWriter(directory, null, data)
    }
    suspend fun serializeResourcesByCountry(directory: String, fileContent: Map<String, List<PersonDTO>>) {
        coroutineScope {
            for (country in fileContent) {
                launch {
                    writeFilesUsingObjectWriter(directory, country.key, country.value)
                }
            }
        }
    }
}
