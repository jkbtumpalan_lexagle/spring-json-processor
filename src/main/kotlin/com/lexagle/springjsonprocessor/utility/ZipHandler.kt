package com.lexagle.springjsonprocessor.utility

import java.io.BufferedInputStream
import java.io.BufferedOutputStream
import java.io.File
import java.io.FileInputStream
import java.io.FileOutputStream
import java.io.IOException
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream
import java.util.zip.ZipOutputStream

object ZipHandler {

    fun unzipFile(zipSrc: String, unzipDest: String) {
        // Create directory if it does not exist
        val dir = File(unzipDest)
        if (!dir.exists()) dir.mkdirs()

        val buffer = ByteArray(1024)

        try {
            val fis = FileInputStream(zipSrc)
            val zis = ZipInputStream(fis)

            var zipEntry = zis.nextEntry

            // Loop zip entries
            while (zipEntry != null) {
                // get entry name and create a file
                val fileName = zipEntry.name
                val newFile = File(unzipDest + File.separator + fileName)

                // Block creation of hidden files (e.g. __MACOSX)
                if (newFile.isHidden) {
                    zis.closeEntry()
                    zipEntry = zis.nextEntry
                    continue
                }

                // Logging of current entry being processed
                println("Unzipping to ${newFile.absolutePath}")

                // Create directories for subdirectories
                File(newFile.parent).mkdirs()

                // Write file content
                val fos = FileOutputStream(newFile)
                var len = zis.read(buffer)
                while (len > 0) {
                    fos.write(buffer, 0, len)
                    len = zis.read(buffer)
                }

                // Refresh Zip Entry
                fos.close()
                zis.closeEntry()
                zipEntry = zis.nextEntry
            }
            zis.closeEntry()
            zis.close()
            fis.close()
        } catch (e: IOException) {
            e.printStackTrace()
        }
    }

    fun zipAll(directory: String, zipFile: String) {
        val sourceFile = File(directory)

        ZipOutputStream(BufferedOutputStream(FileOutputStream(zipFile))).use { it ->
            it.use {
                zipFiles(it, sourceFile, "")
            }
        }
    }

    private fun zipFiles(zipOut: ZipOutputStream, sourceFile: File, parentDirPath: String) {
        val data = ByteArray(2048)

        for (f in sourceFile.listFiles()!!) {
            if (f.isDirectory) {
                val entry = ZipEntry(f.name + File.separator)
                entry.time = f.lastModified()
                entry.isDirectory
                entry.size = f.length()

                zipOut.putNextEntry(entry)

                // Call recursively to add files within this directory
                zipFiles(zipOut, f, f.name)
            } else {
                if (!f.name.contains(".zip")) { // If folder contains a file with extension ".zip", skip it
                    FileInputStream(f).use { fi ->
                        BufferedInputStream(fi).use { origin ->
                            val path = parentDirPath + File.separator + f.name
                            val entry = ZipEntry(path)
                            entry.time = f.lastModified()
                            entry.isDirectory
                            entry.size = f.length()
                            zipOut.putNextEntry(entry)
                            while (true) {
                                val readBytes = origin.read(data)
                                if (readBytes == -1) {
                                    break
                                }
                                zipOut.write(data, 0, readBytes)
                            }
                        }
                    }
                } else {
                    zipOut.closeEntry()
                    zipOut.close()
                }
            }
        }
    }
}
